package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TimerHomePage 
{
	WebDriver driver;
	
	@FindBy(xpath="//input[@name='start_a_timer']")
	public WebElement startTimer_txt;
	
	@FindBy(xpath="//input[@id='timergo']")
	public WebElement go_btn;
	
	@FindBy(xpath="//h2[@id='progressText']")
	public WebElement progressText;
	
	public TimerHomePage(WebDriver driver)
	{
		this.driver = driver;
	    PageFactory.initElements(driver, this);
	}
	
	public void setEnterTime(String setTime) 
	{
		startTimer_txt.clear();
		startTimer_txt.sendKeys(setTime);
	}
	
    public void clickGoButton()
    {
    	go_btn.click();
    }
    
    public String getProgressText()
    {
    	return progressText.getText();
    }
    
    public void getApp(String setTime)
    {
    	this.setEnterTime(setTime);
    	this.clickGoButton();
    }
}
