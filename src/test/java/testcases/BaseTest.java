package testcases;

import static org.testng.Assert.assertEquals;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import browsers.BrowserFactory;
import extentreport.ExtentReport;
import io.qameta.allure.Story;
import pageobjects.TimerHomePage;
import reader.ConfigReader;

public class BaseTest extends BrowserFactory
{
	public ConfigReader reader = new ConfigReader();
	public ExtentReport report = new ExtentReport();
	TimerHomePage timerObj;

	@BeforeTest
	public void setUp()
	{
		getDriver();
		report.extentReportSetup();
	}
	
	@Test(description="Validate E.gg Timer Countdown Time.")
	@Story("Coutdown Timer Application Testing")
	public void getTime()
	{
		try
		{
			report.test = report.extent.createTest("getTime");
			timerObj = new TimerHomePage(driver);
			driver.get(reader.getApplicationUrl());
			timerObj.getApp(reader.getTimeInSecond());
			int seconds = Integer.parseInt(reader.getTimeInSecond());
		    long delay = seconds * 1000;
			do
		    {
		      int sec = seconds % 60;
		      System.out.println("Coutdown Time: "+sec + " second(s)");
		      seconds = seconds - 1;
		      delay = delay - 1000;
		    }
		    while (delay != 0);
			{
			    System.out.println("Time Expired!");
			}
			Thread.sleep(Integer.parseInt(reader.getTimeInSecond())*1000);
			driver.switchTo().alert().accept();			
			System.out.println("Timer End Message is: "+timerObj.getProgressText());
			assertEquals(timerObj.getProgressText(), "Time Expired!", "Text Match");
		}
		catch(Exception e)
		{
			System.out.println("Error in getTime: "+e.getMessage());
		}
	}
	
	@AfterMethod
	public void getStatus() throws Exception
	{
		report.getResult();
	}
	
	@AfterTest
	public void tearDown()
	{
		report.endReport();
		driver.close();
	}
}
